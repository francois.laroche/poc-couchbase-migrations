organization := "com.ingenico"
name         := "couchbase-mogration-poc"
version      := "0.0.1-SNAPSHOT"

Global / scalaVersion := "2.13.8"

lazy val couchmove = project.in(file("couchmove"))

lazy val couchVersion = project.in(file("couchversion"))
