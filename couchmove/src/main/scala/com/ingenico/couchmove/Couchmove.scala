package com.ingenico.couchmove

import com.couchbase.client.java.Bucket
import com.couchbase.client.java.Cluster
import com.couchbase.client.java.manager.bucket.BucketManager
import com.couchbase.client.java.manager.bucket.BucketSettings
import com.github.couchmove.Couchmove

import java.time.Duration

object Couchmove extends App { // 2+ years without commit, no collection
  val username   = "Administrator"
  val password   = "couchbase"
  val bucketName = "couchmove2"

  val cluster = Cluster.connect("couchbase://localhost", username, password)

  val bucket     = getOrCreateBucket(cluster, bucketName)
  val collection = bucket.defaultScope().collection("migrations")
  bucket.waitUntilReady(Duration.ofMinutes(1))

  val couchmove = new Couchmove(collection, cluster, "migrations")
  couchmove.migrate()

  private def getOrCreateBucket(cluster: Cluster, bucketName: String): Bucket = {
    val buckets: BucketManager = cluster.buckets()
    if (!buckets.getAllBuckets.containsKey(bucketName)) {
      buckets.createBucket(BucketSettings.create(bucketName))
    }
    cluster.bucket(bucketName)
  }
}
