name := "couchversion"

libraryDependencies ++= Seq(
  "com.github.couchbaselabs" % "couchversion" % "0.5.1",
  "io.circe"                %% "circe-parser" % "0.14.2"
)
