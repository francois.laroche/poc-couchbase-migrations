package com.ingenico.couchversion

import com.github.couchversion.CouchVersion

object CouchVersion extends App { // 2+ years without commit
  val runner: CouchVersion = Utils.createRunner()
  runner.setChangeLogsScanPackage("com.ingenico.couchversion.migrations")
  runner.execute()
}
