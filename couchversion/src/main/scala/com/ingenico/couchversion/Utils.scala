package com.ingenico.couchversion

import com.couchbase.client.java.Cluster
import com.couchbase.client.java.manager.bucket.BucketSettings
import com.github.couchversion.CouchVersion

import java.time.Duration
import scala.collection.Factory.stringFactory
import scala.io.Source

object Utils {
  val username   = "Administrator"
  val password   = "couchbase"
  val bucketName = "couchversion4"

  val cluster: Cluster = Cluster.connect("couchbase://localhost", username, password)
  createBucketIfNeeded(bucketName)

  def createRunner() = new CouchVersion(cluster, cluster.bucket(bucketName))

  def createBucketIfNeeded(name: String): Unit = {
    val buckets = cluster.buckets()
    if (!buckets.getAllBuckets.containsKey(name)) {
      buckets.createBucket(BucketSettings.create(name))
      cluster.bucket(name).waitUntilReady(Duration.ofMinutes(5))
    }
  }

  def readResource(name: String): String = Source.fromResource(name).to(stringFactory)
}
