package com.ingenico.couchversion.migrations

import com.github.couchversion.changeset.ChangeLog
import com.github.couchversion.changeset.ChangeSet
import com.ingenico.couchversion.Utils
import io.circe.parser._
import io.circe.Decoder
import io.circe.Json

@ChangeLog(order = "00000002")
class ReferenceData {

  @ChangeSet(id = "insertCountries", author = "me", order = "1", runAlways = false)
  def insertCountries(): Unit =
    insertData[Int]("countries.json", "country", "code")

  @ChangeSet(id = "insertCurrencies", author = "me", order = "2", runAlways = false)
  def insertCurrencies(): Unit =
    insertData[String]("currencies.json", "currency", "alpha3")

  @ChangeSet(id = "insertMCC", author = "me", order = "3", runAlways = false)
  def insertMCC(): Unit =
    insertData[String]("merchant_category_codes.json", "merchant_category_code", "mcc")

  private def insertData[IdKind: Decoder](resourceName: String, objectType: String, idFieldName: String): Unit = {
    val data       = Utils.readResource(resourceName)
    val collection = Utils.cluster.bucket(Utils.bucketName).defaultCollection()
    decode[Array[Json]](data).toTry.foreach { elements =>
      elements.foreach { json =>
        val part = json.hcursor.downField(idFieldName).as[IdKind].toOption.get
        collection.insert(s"$objectType::$part", json)
      }
    }
  }
}
