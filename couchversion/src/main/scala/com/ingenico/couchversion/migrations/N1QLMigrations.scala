package com.ingenico.couchversion.migrations

import com.github.couchversion.changeset.ChangeLog
import com.github.couchversion.changeset.ChangeSet
import com.ingenico.couchversion.Utils

@ChangeLog(order = "00000001")
class N1QLMigrations {

  private val bucketName: String = Utils.bucketName

  @ChangeSet(id = "createIndexes", author = "me", order = "0", runAlways = false)
  def createIndexes(): Unit = {
    val script =
      s"""
         |create index type_provisioning on `$bucketName`(`_type`)
         |""".stripMargin
    Utils.cluster.query(script)
  }

  @ChangeSet(id = "migration1", author = "me", order = "1", runAlways = false)
  def migration1(): Unit = {
    val script =
      s"""
         |update $bucketName set customerInfo = { "billingReference": billingReference, "financialInformation": financialInformation} where _type = "organization" and customerInfo is not valued;
         |""".stripMargin
    Utils.cluster.query(script)
  }

  @ChangeSet(id = "migration2", author = "me", order = "2", runAlways = false)
  def migration2(): Unit = {
    val script =
      s"""
         |update $bucketName set isCustomer = false, merchantActivity= false, integratorActivity = false, serviceProviderActivity = false, deviceFleetOperatorActivity = false, resellerActivity= true where _type = "organization" and resellerActivity is not valued;
         |""".stripMargin
    Utils.cluster.query(script)
  }

  @ChangeSet(id = "migration3.1", author = "me", order = "3.1", runAlways = false)
  def migration3_1(): Unit = {
    val script =
      s"""
         |UPDATE `$bucketName` p SET p.location = {"country": "FR"} WHERE p._type = "merchant" AND p.location IS NOT VALUED;
         |""".stripMargin
    Utils.cluster.query(script)
  }

  @ChangeSet(id = "migration3.2", author = "me", order = "3.2", runAlways = false)
  def migration3_2(): Unit = {
    val script =
      s"""
         |UPDATE `$bucketName` p SET p.location.country = "FR" WHERE p._type = "merchant" AND p.location.country IS NOT VALUED;
         |""".stripMargin
    Utils.cluster.query(script)
  }

  @ChangeSet(id = "migration4.1", author = "me", order = "4.1", runAlways = false)
  def migration4_1(): Unit = {
    val script =
      s"""
         |UPDATE `$bucketName` p SET p.location = {"country": "FR"} WHERE p._type = "organization" AND p.location IS NOT VALUED;
         |""".stripMargin
    Utils.cluster.query(script)
  }

  @ChangeSet(id = "migration4.2", author = "me", order = "4.2", runAlways = false)
  def migration4_2(): Unit = {
    val script =
      s"""
         |UPDATE `$bucketName` p SET p.location.country = "FR" WHERE p._type = "organization" AND p.location.country IS NOT VALUED;
         |""".stripMargin
    Utils.cluster.query(script)
  }

}
